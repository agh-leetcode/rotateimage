import java.util.Arrays;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int matrix [][]= {
                {1,2,3},
                {4,5,6},
                {7,8,9},
        };

        int matrix2 [][]= {
                {5, 1, 9,11},
                {2, 4, 8,10},
                {13, 3, 6, 7},
                {15,14,12,16}
        };

        rotate(matrix2);
    }

    //better solution
    private void rotate2(int[][] matrix) {

        for (int i = 0; i<matrix.length/2; i++){
            for (int j = i; j<matrix.length-i-1; j++){
                int temp = matrix[i][j];
                matrix[i][j]=matrix[matrix.length-j-1][i];
                matrix[matrix.length-j-1][i]=matrix[matrix.length-i-1][matrix.length-j-1];
                matrix[matrix.length-i-1][matrix.length-j-1]=matrix[j][matrix.length-i-1];
                matrix[j][matrix.length-i-1]=temp;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " "
                );
            }
            System.out.println();
        }
    }

    private void rotate(int [][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix.length; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i]=temp;
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length/2; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[i][matrix.length-j-1];
                matrix[i][matrix.length-j-1]=temp;
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " "
                );
            }
            System.out.println();
        }
    }
}
